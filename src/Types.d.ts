export type Move = 'rock' | 'paper' | 'scissors'

export interface Bot {
	Name: string
	Report?(myMove: Move, theirMove: Move, result: number): void
	Shoot(): Move
}

export type BotFactory = () => Bot
