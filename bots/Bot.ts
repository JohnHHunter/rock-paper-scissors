import { BotFactory } from '../src/Types.d.ts'

const bot: BotFactory = () => {
    let theirLastMove = ''
    let lastResult = 0

    let myHistory = new Array();
    let theirHistory = new Array();

    let myMoveCounts = new Map();
    myMoveCounts.set('rock', 0)
    myMoveCounts.set('paper', 0)
    myMoveCounts.set('scissors', 0)

    let theirMoveCounts = new Map();
    theirMoveCounts.set('rock', 0)
    theirMoveCounts.set('paper', 0)
    theirMoveCounts.set('scissors', 0)

	let roundNum = 0;

    // Called after each round with the results
    // result is 1 for win, -1 for loss, 0 for tie
    const Report = (myMove: string, theirMove: string, result: int) => {
        lastResult = result
        theirLastMove = theirMove

        myHistory.push(myMove);
        theirHistory.push(theirMove);

        myMoveCounts.set(myMove, myMoveCounts.get(myMove)+1);
        theirMoveCounts.set(theirMove, theirMoveCounts.get(theirMove)+1);

		roundNum++;
    }

	const getOpponentsLeastUsedMove = function (mapIn) {
		var min = 100000;
		var minKey = '';
		for (var key of mapIn.keys()) {
			if (mapIn.get(key) < min)
			{
				min = mapIn.get(key);
				minKey = key;
			}
		}
		return minKey;
		
	  };

    const Shoot = () => {
        // Decide what to do
		if (roundNum % 3 == 0 || roundNum % 3 == 1)
		{
			return getOpponentsLeastUsedMove(theirMoveCounts) || 'rock';
		}
		else
		{
			return 'rock'
		}
    }

    return {
        Name: 'John\'s Hunter 🤖',
        Shoot,
        Report,
    }
}

export default bot
